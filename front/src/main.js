import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import '@/assets/style/tailwind.css'

import socketio from 'socket.io-client'
import axios from 'axios'
import dotenv from 'dotenv'

Vue.config.productionTip = false

const api = process.env.VUE_APP_API

Vue.prototype.$axios = axios
Vue.prototype.$socket = socketio(api)
dotenv.config()

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
