require('dotenv').config()
const express = require('express')
const app = express()
const server = require('http').createServer(app)
const path = require('path')
const cors = require('cors')
const io = require('socket.io').listen(server)
const rug = require('random-username-generator')
const { uuid } = require('uuidv4')
const randomstring = require("randomstring")
const schedule = require('node-schedule')
const NodeCouchDb = require('node-couchdb')
const database = 'hotpotato'
const port = process.env.PORT || 3210

const durationSeconds = 45
const startSeconds = 5

let players = {}

let game = {
    potatoes: [],
    players: [],
    playing: false
}

const playersPerPotato = 2

//  CORS
app.use(cors())

//  COUCHDB
const couch = new NodeCouchDb({
    host: 'humpback02.fig.limited',
    protocol: 'https',
    port: 6984,
    auth: {
        user: process.env.ADMIN_USER,
        pass: process.env.ADMIN_PASSWORD
    }
})

//  GET HTML FILE GENERATED AND COPIED FROM VUE
app.use(express.static(path.join(__dirname, 'public')))
app.get('/', function (req, res) {
    res.render('index')
})

//  WEB SERVER
server.listen(port, function () {
    console.log('Listening on :' + port)
})

//  SCHEDULE
const j = schedule.scheduleJob('* * * * *', function() {

    let seconds = -1
    const remainingSeconds = 5;
    let timeStart

    let timer = setInterval(function () {
        timeStart = new Date()
        currentsSeconds = timeStart.getSeconds()
        if (currentsSeconds !== seconds) {
            if (seconds === remainingSeconds - 1) {
                clearInterval(timer)
                //  put ready players into game object
                game.players = Object.values(players).filter(player => player.ready)

                let unchosenPlayers = {
                    ...game.players
                }

                //  get potato count
                const potatoesQuantity = parseInt(game.players.length / playersPerPotato)

                //  potatoes
                for(let p = 0; p < potatoesQuantity; p++) {
                    const r = random(0, players2.length)
                    game.potatoes[p] = players2.splice(r, 1)
                }

                console.log('start', game, timeStart.toISOString())
                io.emit('start', game)
            } else {
                console.log('starting', game, remainingSeconds - currentsSeconds, timeStart.toISOString())
                io.emit('starting', game, remainingSeconds - currentsSeconds)
                seconds = currentsSeconds
            }
        }
    }, 100);
    
    
    //  number of players
    // game.players = Object.values(players).filter(player => player.ready)
    // const potatoesQuantity = parseInt(game.players.length / playersPerPotato)

    // console.log(Object.values(players))
    // let players2 = {
    //     ...game.players
    // }

    // //  potatoes
    // for(let p = 0; p < potatoesQuantity; p++) {
    //     const r = random(0, players2.length)
    //     game.potatoes[p] = players2.splice(r, 1)
    // }

    // io.emit('start', game)





    // if (playing) {
    //     //  pick a random player
    //     const gamePlayers = Object.values(players).filter(player => player.ready)
    //     console.log('gamePlayers', gamePlayers)

    //     const starter = gamePlayers[Math.floor(Math.random() * gamePlayers.length)]

    //     console.log('starter', starter)
    // }
    // const d = new Date()
    // console.log('playing', playing, d.toISOString())
})

/*******************************************************************************
 * MAIN SOCKET EVENT SECTION
 ******************************************************************************/
io.sockets.on('connection', function (socket)
{
    socketConnect(socket)

    socket.on('disconnect', (_) => socketDisconnect(socket))

    socket.on('get-player', (apiToken) => socketGetPlayer(socket, apiToken))
    
    socket.on('ready', (state) => socketReady(socket, state))

    socket.on('pass', (_) => socketPass(socket))
})

/**
 * socketConnect
 * @param {*} socket 
 */
function socketConnect (socket)
{
    console.log('socketConnect', socket.id)
    players[socket.id] = {}
    console.log('socketConnect', players)
    socket.emit('connected')
}

/**
 * socketDisconnect
 * @param {*} socket 
 */
function socketDisconnect (socket)
{
    console.log('socketDisconnect', socket.id)
    delete players[socket.id]
    socket.emit('disconnected')
}

/**
 * socketGetPlayer
 * @param {*} socket 
 * @param {*} apiToken 
 */
function socketGetPlayer(socket, apiToken)
{
    const view = '/_design/all_players/_view/by_api?key="' + apiToken + '"&include_docs=true'
    couch.get(database, view).then(({data, headers, status}) => {
        if(data.rows.length === 0) {
            const newApiToken = uuid()
            const username = rug.generate().toLowerCase()
            const doc = {
                apiToken: newApiToken,
                username
            }
            couch.insert(database, doc)
            .then(({data, headers, status}) => {
                players[socket.id] = doc
                socket.emit('set-player', doc)
                console.log('game', game)
                socket.emit('game', game)
                io.emit('new-player', socket.id, doc)
            }, err => {
                console.error(err)
                // either request error occured
                // ...or err.code=EDOCCONFLICT if document with the same id already exists
            })
        } else {
            console.log('existing couch player')
            players[socket.id] = data.rows[0].value
            socket.emit('set-player', data.rows[0].value)
            console.log('game', game)
            socket.emit('game', game)
            io.emit('new-player', socket.id, data.rows[0].value)
        }
    }, err => {
        console.error('error getting couch player', err)
        if (err.code === 'EDOCMISSING') {
            const newApiToken = uuid()
            const username = rug.generate().toLowerCase()
            const doc = {
                apiToken: newApiToken,
                username
            }
            couch.insert(database, doc)
            .then(({data, headers, status}) => {
                players[socket.id] = doc
                socket.emit('set-player', doc)
                console.log('game', game)
                socket.emit('game', game)
                io.emit('new-player', socket.id, doc)
            }, err => {
                console.error(err)
                // either request error occured
                // ...or err.code=EDOCCONFLICT if document with the same id already exists
            })
        }
    })
}

/**
 * socketReady
 * @param {*} socket 
 * @param {*} state 
 */
function socketReady(socket, state)
{
    console.log(game.players)
    players[socket.id].ready = state
    io.emit('new-player-state', socket.id, state)
    // players[socket.id].ready = state
    // players[socket.id].socketId = socket.id
    console.log('ready', socket.id, state)
}

/**
 * socketPass
 * @param {*} socket 
 */
function socketPass(socket)
{
    // players[socket.id].state = state
    console.log('pass', socket.id)
}

// RANDOM ITEM FROM ARRAY
function random(mn, mx) {  
    return Math.random() * (mx - mn) + mn;  
} 
