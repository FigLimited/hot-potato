require('dotenv').config()
const express = require('express')
const app = express()
const server = require('http').createServer(app)
const path = require('path')
const cors = require('cors')
const io = require('socket.io').listen(server)
const rug = require('random-username-generator')
const { uuid } = require('uuidv4')
const randomstring = require("randomstring")
const schedule = require('node-schedule')
const NodeCouchDb = require('node-couchdb')
const database = 'hotpotato'
const port = process.env.PORT || 3210

//  CORS
app.use(cors())

//  COUCHDB
const couch = new NodeCouchDb({
    host: 'humpback02.fig.limited',
    protocol: 'https',
    port: 6984,
    auth: {
        user: process.env.ADMIN_USER,
        pass: process.env.ADMIN_PASSWORD
    }
})

//  GET HTML FILE GENERATED AND COPIED FROM VUE
app.use(express.static(path.join(__dirname, 'public')))
app.get('/', function (req, res) {
    res.render('index')
})

//  WEB SERVER
server.listen(port, '0.0.0.0', function () {
    console.log('Listening on :' + port)
})

//  SENT TO THE CLIENT
let game = {
    attendees: {},
    players: [],
    potatoes: [],
    playing: false,
    countdown: null
}

//  GAME VARIABLES
const durationSeconds = 40
const startSeconds = 5
const playersPerPotato = 5



//  SCHEDULE
const j = schedule.scheduleJob('* * * * *', function ()
{
    let lastSeconds = 0
    let timer = setInterval(function () {
        const timeStart = new Date()
        const currentSeconds = timeStart.getSeconds()
        if (lastSeconds !== currentSeconds) {
            if (currentSeconds === startSeconds + 1) {
                clearInterval(timer)
    
                game.countdown = null
                game.playing = true
                game.lastPass = timeStart

                //  put ready players into game object
                game.players = Object.values(game.attendees).filter(player => player.ready)
                
                if (game.players.length > 1) {

                    //  get potato count
                    const potatoesQuantity = parseInt(game.players.length / playersPerPotato) + 1
    
                    let unchosenPlayers = [
                        ...game.players
                    ]
                    console.log('unchosenPlayers', game.players, unchosenPlayers)
                    
                    //  potatoes
                    for(let p = 0; p < potatoesQuantity; p++) {
                        const r = random(0, unchosenPlayers.length)
                        game.potatoes[p] = unchosenPlayers.splice(r, 1)[0]
                    }
                    console.log('game.potatoes', game.potatoes)
    
                    io.emit('start', game)
                    setTimeout(endRound, durationSeconds * 1000)
                } else {
                    endRound()
                }



            } else {
                game.countdown = startSeconds - currentSeconds
                io.emit('starting', game)
            }
            lastSeconds = currentSeconds
        }
    }, 50)






    // const timeStart = new Date()
    // const cur
    // io.emit('starting')




    //     //  reset from previous round
    //     game.players = []
    //     game.potatoes = []



    // let seconds = -1
    // let timeStart
    // let timeElapsed = 0

    // let timer = setInterval(function () {
    //     timeStart = new Date()
    //     currentSeconds = timeStart.getSeconds()
    //     console.log('currentSeconds', currentSeconds, startSeconds)
    //     if (currentSeconds === startSeconds) {
    //         clearInterval(timer)
    //         game.countdown = 0
    //         //  put ready players into game object
    //         game.players = Object.values(game.attendees).filter(player => player.ready)
    //         let unchosenPlayers = Object.keys(game.attendees).filter(player => player.ready)
    //         console.log('game.players', game.players, game.attendeess, unchosenPlayers)
    //         //  get potato count
    //         // const potatoesQuantity = parseInt(game.players.length / playersPerPotato)
    //         // //  potatoes
    //         // for(let p = 0; p < potatoesQuantity; p++) {
    //         //     const r = random(0, unchosenPlayers.length)
    //         //     let chosenPlayer = unchosenPlayers.splice(r, 1)
    //         //     console.log('chosenPlayer', chosenPlayer, r, unchosenPlayers.length)
    //         //     game.potatoes.push(chosenPlayer)
    //         //     playerIndex = game.players.findIndex((pl => pl.socketd === chosenPlayer.socketId))
    //         //     game.players[playerIndex].elapsed = 0
    //         // }

    //         // game.playing = true
    //         // console.log('game.potatoes', game.potatoes)
    //         // io.emit('start', game)
            
    //         setTimeout(endRound, durationSeconds * 1000)

    //     } else {
    //         console.log('starting', startSeconds - currentSeconds)
    //         game.countdown = startSeconds - currentSeconds
    //         io.emit('starting', game )
    //     }
    // }, 100)
})


/*******************************************************************************
 * MAIN SOCKET EVENT SECTION
 ******************************************************************************/
io.sockets.on('connection', function (socket)
{
    socketConnect(socket)

    socket.on('disconnect', (_) => socketDisconnect(socket))

    socket.on('get-player', (apiToken) => socketGetPlayer(socket, apiToken))
    
    socket.on('ready', (state) => socketReady(socket, state))

    socket.on('pass', (_) => socketPass(socket))
})

/**
 * socketConnect
 * @param {*} socket 
 */
function socketConnect (socket)
{
    console.log('socketConnect', socket.id)
    game.attendees[socket.id] = {
        socketId: socket.id,
        elapsed: 0,
        ready: false
    }
    socket.emit('connected', game)
    io.emit('game', game)
}

/**
 * socketDisconnect
 * @param {*} socket 
 */
function socketDisconnect (socket)
{
    console.log('socketDisconnect', socket.id)
    delete game.attendees[socket.id]
    socket.emit('disconnected')
    io.emit('game', game)
}

/**
 * socketGetPlayer
 * @param {*} socket 
 * @param {*} apiToken 
 */
function socketGetPlayer(socket, apiToken)
{
    console.log('getPlayer')
    const view = '/_design/all_players/_view/by_api?key="' + apiToken + '"&include_docs=true'
    couch.get(database, view).then(({data, headers, status}) => {
        if(data.rows.length === 0) {
            // console.log('data.rows.length === 0')
            const newApiToken = uuid()
            const username = rug.generate().toLowerCase()
            const doc = {
                apiToken: newApiToken,
                username
            }
            couch.insert(database, doc)
            .then(({data, headers, status}) => {
                doc.ready = false
                doc.elapsed = 0
                game.attendees[socket.id] = doc
                socket.emit('set-player', doc)
                // console.log('game', game)
                // io.emit('new-player', socket.id, doc)
            }, err => {
                console.error(err)
                // either request error occured
                // ...or err.code=EDOCCONFLICT if document with the same id already exists
            })
        } else {
            // game.attendees[socket.id] = data.rows[0].value
            // socket.emit('me', game)
            // io.emit('new-player', socket.id, data.rows[0].value)
            game.attendees[socket.id].username = data.rows[0].value.username
            game.attendees[socket.id].ready = false
            game.attendees[socket.id].elapsed = 0
            console.log('existing couch player', game.attendees)
            socket.emit('set-player', data.rows[0].value)
        }
    }, err => {
        console.error('error getting couch player', err)
        if (err.code === 'EDOCMISSING') {
            const newApiToken = uuid()
            const username = rug.generate().toLowerCase()
            const doc = {
                apiToken: newApiToken,
                username
            }
            couch.insert(database, doc)
            .then(({data, headers, status}) => {
                // game.attendees[socket.id] = doc
                doc.ready = false
                doc.elapsed = 0
                game.attendees[socket.id] = doc
                socket.emit('set-player', doc)
                // console.log('game', game)
                // socket.emit('game', game)
                // io.emit('new-player', socket.id, doc)
            }, err => {
                console.error(err)
                // either request error occured
                // ...or err.code=EDOCCONFLICT if document with the same id already exists
            })
        }
    })
}

/**
 * socketReady
 * @param {*} socket 
 * @param {*} state 
 */
function socketReady(socket, state)
{
    console.log('socketReady', socket.id, state)
    game.attendees[socket.id].ready = state
    game.attendees[socket.id].elapsed = 0
    io.emit('game', game)
    // console.log('ready', socket.id, state)
}

/**
 * socketPass
 * @param {*} socket 
 */
function socketPass(socket)
{
    console.log('socketPass', game.players)
    if (!game.playing) {
        return
    }

    const lastPass = game.lastPass
    const now = new Date()
    const diff = now - lastPass
    const playerIndex = game.players.findIndex((pl => pl.socketId === socket.id))
    game.players[playerIndex].elapsed += diff

    let unchosenPlayers = [
        ...game.players
    ]

    //  remove players that currently have a potato
    for(let p = 0; p < game.potatoes.length; p++) {
        const index = unchosenPlayers.findIndex(player => player.socketId === game.potatoes[p].socketId)
        unchosenPlayers.splice(index, 1)
    }

    //  pick a new player
    for(let p = 0; p < game.potatoes.length; p++) {
        if (game.potatoes[p].socketId === socket.id) {
            const r = random(0, unchosenPlayers.length)
            game.potatoes[p] = unchosenPlayers.splice(r, 1)[0]
        }
    }

    io.emit('game', game)

    game.lastPass = now
}

/**
 * endRound
 */
function endRound()
{
    console.log('ending round', game.attendees)
    game.playing = false

    //  set everyone to not ready
    const attendees = Object.keys(game.attendees)
    for (let a = 0; a < attendees.length; a++) {
        game.attendees[attendees[a]].ready = false
    }

    io.emit('end', game)
}

// RANDOM ITEM FROM ARRAY
function random(mn, mx) {  
    return Math.random() * (mx - mn) + mn;  
} 
