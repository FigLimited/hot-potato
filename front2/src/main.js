import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import '@/assets/style/tailwind.css'

import socketio from 'socket.io-client'
import axios from 'axios'
import dotenv from 'dotenv'

Vue.config.productionTip = false

const api = process.env.VUE_APP_API

Vue.prototype.$axios = axios
Vue.prototype.$socket = socketio(api)

dotenv.config()

new Vue({
  router,
  store,
  data: {
    game: {
      players: []
    },
    player: null,
    playing: false,
    ready: false,
    loading: {
      canCancel: false,
      isLoading: false,
      isFullPage: true
    }
  },
  created () {
    console.log('created')
    //  local event
    this.$socket.on('disconnect', (reason) => {
      console.log('disconnect', reason)
      this.connected = false
    })
    //  remote events
    this.$socket.on('connected', (players) => {
      console.log('connected to socket server', players)
      if (this.player === null || this.player === undefined) {
        this.getPlayer()
      }
      this.connected = true
    })
    //  disconnected
    this.$socket.on('disconnected', (_) => {
      console.log('disconnected from socket server', this.$socket.id)
      this.connected = false
    })
    //  set-player
    this.$socket.on('set-player', (player) => {
      console.log('set-player', player)
      // this.player = player
      localStorage.apiToken = player.apiToken
      this.player = player
      this.loading.isLoading = false
    })
    //  start
    this.$socket.on('start', (game) => {
      console.log('start', game)
      game.players.map(player => {
        if (this.$socket.id === player.socketId) {
          this.playing = true
        }
      })
    })
    //  new player state
    this.$socket.on('new-player-state', (socketId, state) => {
      console.log(this.game)
      const objIndex = this.game.players.findIndex(player => player.socketId === socketId)
      this.game.players[objIndex].ready = state
    })

    //  game
    this.$socket.on('game', (game) => {
      console.log('game', game)
      this.game = game
    })

    //  game-end
    this.$socket.on('game-end', (game) => {
      console.log('game-end', game)
      this.game = game
      this.ready = false
    })

    //  new player
    // this.$socket.on('new-player', (socketId, player) => {
    //   console.log('new-player', player)
    //   this.game.players[socketId] = player
    // })
  },
  methods: {
    getPlayer () {
      console.log('getPlayer', localStorage.apiToken)
      this.$socket.emit('get-player', localStorage.apiToken)
    }
  },
  render: h => h(App)
}).$mount('#app')
